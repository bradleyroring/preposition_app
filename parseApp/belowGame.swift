//
//  belowGame.swift
//  parseApp
//
//  Created by D. Manning on 11/30/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import Parse

class belowGame: UIViewController, UITextFieldDelegate {
    
    var viewDrag: UIView!
    var circle: UIView!
    
    var arrayOfNames = ["right","between","above"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        count += 1
        circle = UIView(frame: CGRect(x:Int(arc4random_uniform(200)), y:Int(arc4random_uniform(50) + 100), width:100, height:100))
        circle.backgroundColor = UIColor.orange
        self.view.addSubview(circle)
        
        viewDrag = UIView(frame: CGRect(x:150, y:450, width:100, height:100))
        viewDrag.backgroundColor = UIColor.blue
        self.view.addSubview(viewDrag)
        var panGesture = UIPanGestureRecognizer()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(rightGame.draggedView(_:)))
        viewDrag.isUserInteractionEnabled = true
        viewDrag.addGestureRecognizer(panGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        self.view.bringSubview(toFront: viewDrag)
        let translation = sender.translation(in: self.view)
        viewDrag.center = CGPoint(x: viewDrag.center.x + translation.x, y: viewDrag.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func buttonPush(_ sender: Any) {
        
        if((viewDrag.center.y > circle.center.y) && (viewDrag.center.x >= circle.center.x - 45 && viewDrag.center.x <= circle.center.x + 45)){
            currentScore += 1
        }
        if(count == 8){
            let vcName = "finish"
            let vc = storyboard?.instantiateViewController(withIdentifier: vcName)
            self.present(vc!, animated: true, completion: nil)
            
        }
        let numberOfNames = UInt32(arrayOfNames.count)
        let randomNumber = Int(arc4random() % numberOfNames)
        let vcName = arrayOfNames[randomNumber]
        let vc = storyboard?.instantiateViewController(withIdentifier: vcName)
        self.present(vc!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

