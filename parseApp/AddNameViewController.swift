//
//  AddNameViewController.swift
//  parseApp
//
//  Created by D. Manning on 10/27/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import Parse

class AddNameViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameText: UITextField!
    var currentUser = PFUser.current()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.nameText.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        let name = PFObject(className:"Name")
        name["name"] = nameText.text
        name["email"] = PFUser.current()?["email"] as? String
        name.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                // The object has been saved.
                print("great jorb")
            } else {
                // There was a problem, check error.description
            }
        }
      //  performSegue(withIdentifier: "Save Name", sender: self)
    }
}

