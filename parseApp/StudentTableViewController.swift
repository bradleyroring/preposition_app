//
//  StudentTableViewController.swift
//  parseApp
//
//  Created by D. Manning on 10/27/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import Parse

class StudentTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var studen = ""
    
    var currentUser = PFUser.current()
    var user: PFObject = PFObject(className:"Name")
    var allNames: Array<PFObject> = Array<PFObject>()
 
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        let query = PFQuery(className:"Name")
        query.whereKey("email", equalTo: PFUser.current()?["email"] as? String)
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        self.allNames.append(object)
                        print(object.objectId!)
                    }
                    print(self.allNames)
                }
            } else {
                //fail
                print("and if failed")
            }
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logoutButtonPush(_ sender: UIBarButtonItem) {
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String! as Any )
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(allNames.count)
        return allNames.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellName", for: indexPath)
        let name = allNames[indexPath.row]

        cell.textLabel?.text = (name).object(forKey: "name") as? String
        
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        user = allNames[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)//deselect row
    }*/
    
    //swipe left to delete contact
  /*  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            let deleteName = allNames[indexPath.row]
            let query = PFQuery(className: "Name")
            query.whereKey("name", equalTo: deleteName["name"])
            query.findObjectsInBackground { (objects, error) in
                for object in objects! {
                    object.deleteEventually()
                }
            }
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }*/
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loadStudent" {
            let destination = segue.destination as! Student
            let indexPath = tableView.indexPathForSelectedRow
            let currentCell = tableView.cellForRow(at: indexPath!)
            destination.studen = (currentCell?.textLabel?.text)!
        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        tableView.reloadData()
    }
}
