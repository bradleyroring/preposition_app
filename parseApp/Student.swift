//
//  Student.swift
//  parseApp
//
//  Created by D. Manning on 11/26/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import Parse
var stay = ""
var count = 0
var currentScore = 0
class Student: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var laScore: UILabel!
    @IBOutlet weak var avScore: UILabel!
    
    var studen = ""
    var arrayOfNames = ["above","right","between"]
    var allScores: Array<PFObject> = Array<PFObject>()
    
    var currentUser = PFUser.current()
    var user: PFObject = PFObject(className:"Score")
    
    @IBOutlet weak var nameText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if (studen != ""){stay = studen}
        nameText.text = stay
        
        //Last test data
        let query = PFQuery(className: "Score")
        query.whereKey("email", equalTo: PFUser.current()?["email"] as? String)
        query.whereKey("name", equalTo: stay)
        query.order(byDescending: "createdAt")
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
            } else {
                // The find succeeded.
                self.user = object!
                print("Successfully retrieved the object.")
                let myScore = self.user.object(forKey: "thisScore")
                print(self.user.object(forKey: "thisScore")!)
                var myString = ""
                if let v = myScore {
                    myString = "\(v)"
                }
                self.laScore.text = myString
            }
        }
        
        //Average Test Data
        let avquery = PFQuery(className: "Score")
        avquery.whereKey("email", equalTo: PFUser.current()?["email"] as? String)
        avquery.whereKey("name", equalTo: stay)
        avquery.findObjectsInBackground { (objects, error) in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        self.allScores.append(object)
                        print(object.objectId!)
                    }
                    print(self.allScores)
                    let scoreArray = self.allScores.map { $0.object(forKey: "thisScore") }
                    var avg = 0.00
                    for i in 0 ..< scoreArray.count{
                        var myString = ""
                        if let v = scoreArray[i] {
                            myString = "\(v)"
                        }
                        let myInt = Double(myString)
                        avg += myInt!
                    }
                    if(scoreArray.count != 0){
                    var temp = 0.00
                        temp = avg / Double(scoreArray.count)
                    self.avScore.text = String(format: "%.2f", temp)
                    }
                }
            } else {
                print("and it failed")
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonPush(_ sender: Any) {
        let numberOfNames = UInt32(arrayOfNames.count)
        let randomNumber = Int(arc4random() % numberOfNames)
        let vcName = arrayOfNames[randomNumber]
        let vc = storyboard?.instantiateViewController(withIdentifier: vcName)
        self.present(vc!, animated: true, completion: nil)
    }
    
}



