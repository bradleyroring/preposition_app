//
//  SignUpViewController.swift
//  parseApp
//
//  Created by D. Manning on 10/27/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
  //  var currentUser = PFUser.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailText.delegate = self
        self.passwordText.delegate = self
    }
    //make keyboard go away on tap
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func signUpButtonTap(_ sender: UIButton) {
        //Sign UP
        let user = PFUser()
        user.password = passwordText.text!
        user.username = emailText.text!
        user.email = emailText.text!
        
        // other fields can be set just like with PFObject
        user.signUpInBackground { (succeeded, error) in
            if error != nil {
                print(error ?? NSString())
            } else {
               // self.currentUser = user
                print("good jorb")
            }
        }
       // performSegue(withIdentifier: "Done Sign", sender: self)
    }
}

